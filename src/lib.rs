#![feature(int_to_from_bytes)]
//#![no_std]
#![feature(alloc)]
#![feature(never_type)]
extern crate core;


use core::cmp::Ordering;

mod internal;

pub type PageId = u32;
pub type PageBuf = [u32; 128];
pub type TreeKey = u32;

pub type Failable<E> = Result<(), E>;

pub const UNDEFINED_PAGE: PageId = 0;

pub trait StorageLayer<E> {
    fn get_root_id(&self) -> Result<PageId, E>;
    fn put_root_id(&mut self, root_id: PageId) -> Failable<E>;

    fn get_page(&self, page_id: PageId, buffer: &mut PageBuf) -> Failable<E>;
    fn put_page(&mut self, page_id: PageId, buffer: &mut PageBuf) -> Failable<E>;

    fn alloc_page(&mut self) -> Result<PageId, E>;
    fn free_page(&mut self, page_id: PageId) -> Failable<E>;
}

pub trait Searcher<E> {
    /// Compare mystery item described by this searcher against `key` -- $ mystery (<=>) key $
    fn compare(&self, key: TreeKey) -> Result<Ordering, E>;
}

pub trait Comparator<E> {
    /// $ a (<=>) b $
    fn compare(&self, a: TreeKey, b: TreeKey) -> Result<Ordering, E>;
}


pub struct KeyIdentitySearch {
    key: TreeKey
}
impl Searcher<!> for KeyIdentitySearch {
    fn compare(&self, key: u32) -> Result<Ordering, !> {
        Ok(self.key.cmp(&key))
    }
}

pub struct KeyCompareSearch<'a, E: 'a> {
    key: TreeKey,
    compare: &'a Comparator<E>
}
impl<'a, E> Searcher<E> for KeyCompareSearch<'a, E> {
    fn compare(&self, key: u32) -> Result<Ordering, E> {
        self.compare.compare(self.key, key)
    }
}



pub struct BlockBTree<'a, E, S: StorageLayer<E> + 'a, C: Comparator<E> + 'a> {
    pub storage: &'a mut S,
    pub comparator: &'a C,
    pub _wat: core::marker::PhantomData<E>
}

impl<'a, E, S: StorageLayer<E> + 'a, C: Comparator<E>> BlockBTree<'a, E, S, C> {
    pub fn insert(&mut self, item: TreeKey) -> Failable<E> {
        internal::insert(self.storage, self.comparator, &mut [0u32; 128], item)
    }

    pub fn retrieve<F: Searcher<E>>(&self, search: &F) -> Result<Option<TreeKey>, E> {
        internal::retrieve(self.storage, search, &mut [0u32; 128])
    }

    pub fn delete(&mut self, item: TreeKey) -> Failable<E> {
        internal::delete(self.storage, self.comparator, &mut [0u32; 128], item)
    }
}