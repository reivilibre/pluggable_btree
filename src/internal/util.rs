use std::ops::Range;

/// Delete items in a slice and move down the others. Preserves order.
pub fn delete<E: Copy>(slice: &mut [E], delete: Range<usize>, blank_ele: E) {
    let del_len = delete.end - delete.start;
    let slice_len = slice.len();


    // first move down items.
    let dest_start = delete.start;
    let source_start = delete.end;

    for (source, dest) in (source_start..slice_len).zip(dest_start..) {
        slice[dest] = slice[source];
    }

    // now blank the last part of the array.
    let blank_start = slice_len - del_len;

    for index in blank_start..slice_len {
        slice[index] = blank_ele;
    }
}


#[cfg(test)]
mod tests {
    #[test]
    fn delete() {
        let x1 = [41, 42, 43, 44];

        let mut x2 = x1.clone();
        super::delete(&mut x2, 0..2, -42);
        assert_eq!([43, 44, -42, -42], x2);

        let mut x2 = x1.clone();
        super::delete(&mut x2, 0..4, -42);
        assert_eq!([-42, -42, -42, -42], x2);

        let mut x2 = x1.clone();
        super::delete(&mut x2, 2..4, -42);
        assert_eq!([41, 42, -42, -42], x2);
    }
}