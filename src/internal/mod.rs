use super::*;

use core::ops::Range;

mod util;

/*

Terminology from Bayer, McCreight 1972:

r — Root
u — Undefined
s — Does not serve any purpose for retrieval but is used for insertion algorithm.

x_1 ... x_i — Keys
p_0 ... p_i — Children Pointers

*/


pub enum CursoredRetrieval {
    Found {
        key: TreeKey,
        key_index: usize,
    },
    NotFound {
        insert_at: usize,
    },
}

pub struct Page<'a> {
    buffer: &'a mut PageBuf,
    children: Range<usize>,
    keys: Range<usize>,
}

pub enum ChopResult {
    /// The item was found exactly at the specified index.
    Equal(usize),
    /// The item wasn't found but may be a child in the specified child node.
    PotentialChildOf(usize),
}

#[derive(Copy, Clone)]
pub struct PageCursorElement {
    page: PageId,
    child_index: u8,
}

impl Default for PageCursorElement {
    fn default() -> Self {
        PageCursorElement {
            page: 0,
            child_index: 0,
        }
    }
}

pub struct PageCursor {
    current_page: PageId,
    stack: [PageCursorElement; layout::MAX_STACK_DEPTH],
    stack_length: usize,
}

impl PageCursor {
    fn new(current_page: PageId) -> Self {
        PageCursor {
            current_page,
            stack: [Default::default(); 64],
            stack_length: 0,
        }
    }

    fn descend_into_page(&mut self, child_index: usize, page_id: PageId) {
        self.stack[self.stack_length] = PageCursorElement {
            page: self.current_page,
            child_index: child_index as u8,
        };
        self.stack_length += 1;
        self.current_page = page_id;
    }

    fn ascend_parent(&mut self) -> PageCursorElement {
        self.stack_length -= 1;
        let latest = self.stack[self.stack_length];
        self.current_page = latest.page;
        latest
    }

    fn reset(&mut self, reset_page: PageId) {
        self.stack_length = 0;
        self.current_page = reset_page;
    }

    #[inline]
    fn current(&self) -> PageId {
        self.current_page
    }

    #[inline]
    fn len(&self) -> usize {
        self.stack_length
    }
}

#[derive(Copy, Clone)]
pub struct PageEntry {
    pub pointer: PageId,
    pub key: TreeKey,
}

impl<'a> Page<'a> {
    fn from_buf(buffer: &'a mut PageBuf) -> Self {
        let mut p = Page {
            buffer,
            children: layout::CHILD_OFFSET..(layout::CHILD_OFFSET + layout::PARENT_CHILDREN),
            keys: layout::KEY_OFFSET..(layout::KEY_OFFSET + layout::PARENT_KEYS),
        };

        if p.is_leaf() {
            p.children = 0..0;
            p.keys = layout::KEY_OFFSET..(layout::KEY_OFFSET + layout::LEAF_KEYS);
        }
        p
    }

    fn create_in_buf(buffer: &'a mut PageBuf, create_leaf: bool) -> Self {
        let mut header = [0; 4];
        if create_leaf {
            header[layout::HEADER_FLAGS] |= layout::HEADER_FLAG_LEAF;
        }

        for i in 0..buffer.len() {
            buffer[i] = 0;
        }

        buffer[layout::HEADER_OFFSET] = u32::from_be(u32::from_bytes(header));

        Self::from_buf(buffer)
    }

    fn is_leaf(&self) -> bool {
        let h = self.header_bytes();
        h[layout::HEADER_FLAGS] & layout::HEADER_FLAG_LEAF > 0
    }

    fn header_bytes(&self) -> [u8; 4] {
        self.buffer[0].to_be().to_bytes()
    }

    fn set_header_bytes(&mut self, new: [u8; 4]) {
        self.buffer[0] = u32::from_be(u32::from_bytes(new));
    }

    fn children(&mut self) -> &mut [PageId] {
        &mut self.buffer[self.children.clone()]
    }

    fn keys(&mut self) -> &mut [TreeKey] {
        &mut self.buffer[self.keys.clone()]
    }

    fn num_entries(&self) -> u8 {
        self.header_bytes()[layout::HEADER_NUMENTRIES]
    }

    fn set_num_entries(&mut self, new: u8) {
        let mut h = self.header_bytes();
        h[layout::HEADER_NUMENTRIES] = new;
        self.set_header_bytes(h);
    }

    fn chop<E, S: Searcher<E>>(&self, search: &S) -> Result<Option<ChopResult>, E> {
        let mut left = 1;
        let mut right = self.num_entries() as usize - 1;

        while left <= right {
            let mid = (left + right) / 2;
            let entry = self.get_entry(mid);
            match search.compare(entry.key)? {
                Ordering::Less => { // mystery < key
                    if left == mid {
                        return Ok(Some(ChopResult::PotentialChildOf(left - 1)));
                    } else {
                        right = mid - 1;
                    }
                }
                Ordering::Equal => {
                    return Ok(Some(ChopResult::Equal(mid)));
                }
                Ordering::Greater => { // mystery > key
                    if right == mid {
                        return Ok(Some(ChopResult::PotentialChildOf(right)));
                    } else {
                        left = mid + 1;
                    }
                }
            }
        }
        Ok(None)
    }

    fn max_entries(&self) -> usize {
        if self.is_leaf() {
            layout::LEAF_KEYS + 1
        } else {
            layout::PARENT_KEYS + 1
        }
    }

    fn get_entry(&self, index: usize) -> PageEntry {
        if self.is_leaf() {
            if index == 0 {
                PageEntry {
                    pointer: 0,
                    key: UNDEFINED_PAGE,
                }
            } else {
                PageEntry {
                    pointer: 0,
                    key: self.buffer[self.keys.clone()][index - 1],
                }
            }
        } else if index == 0 {
            // the first entry in a non-leaf doesn't have a key.
            PageEntry {
                pointer: self.buffer[self.children.clone()][index],
                key: 0,
            }
        } else {
            PageEntry {
                pointer: self.buffer[self.children.clone()][index],
                key: self.buffer[self.keys.clone()][index - 1],
            }
        }
    }

    fn put_entry(&mut self, index: usize, entry: PageEntry) {
        if self.is_leaf() {
            assert_eq!(entry.pointer, 0, "Entries in leaves can not have a pointer.");
            if index > 0 {
                self.buffer[self.keys.clone()][index - 1] = entry.key;
            } else {
                assert_eq!(entry.key, 0, "The 0th entry can't have a key.");
            }
        } else if index == 0 {
            assert_eq!(entry.key, 0, "The 0th entry can't have a key.");
            self.buffer[self.children.clone()][index] = entry.pointer;
        } else {
            self.buffer[self.keys.clone()][index - 1] = entry.key;
            self.buffer[self.children.clone()][index] = entry.pointer;
        }
    }

    fn append_entry(&mut self, entry: PageEntry) {
        let next_idx = self.num_entries();
        // increment the count
        self.set_num_entries(next_idx + 1);
        self.put_entry(next_idx as usize, entry);
    }

    fn insert_entry(&mut self, insert_at: usize, entry: PageEntry) {
        let num_entries = self.num_entries();
        for index in insert_at..num_entries as usize {
            let entry = self.get_entry(index);
            self.put_entry(index + 1, entry);
        }
        self.put_entry(insert_at, entry);
        self.set_num_entries(num_entries + 1);
    }

    fn delete_entries(&mut self, indices: Range<usize>) {
        assert!(indices.end > indices.start, "deletion indices out of bound: ({}..{}) is reversed.", indices.start, indices.end);
        let num_entries = self.num_entries();
        assert!(indices.end as u8 <= num_entries, "deletion indices out of bound: ({}..{}).end > № of entries ({}).", indices.start, indices.end, num_entries);

        let keys_indices = ((indices.start).max(1) - 1)..(indices.end - 1);
        util::delete(self.keys(), keys_indices, 0);
        if !self.is_leaf() {
            util::delete(self.children(), indices.clone(), 0);
        }
        self.set_num_entries(num_entries - indices.len() as u8);
    }

    fn fission(&mut self, target: &mut Page, incident_entry: PageEntry, entry_insert: usize) -> PageEntry {
        assert_eq!(target.num_entries(), 0, "Target page in a fission must not have any entries.");
        let tot_entries = self.max_entries() + 1;
        let mid = tot_entries / 2;

        let emitted_entry;


        // this will take on mid's pointer later
        target.append_entry(PageEntry {
            pointer: UNDEFINED_PAGE,
            key: 0,
        });

        let num_entries = self.num_entries();

        if entry_insert <= mid {
            // we can move the right-hand half as-is.

            for index in mid..num_entries as usize {
                target.append_entry(self.get_entry(index));
            }
            self.insert_entry(entry_insert, incident_entry);
            emitted_entry = self.get_entry(mid);
            self.delete_entries(mid..num_entries as usize);
        } else {
            // the insertion is in the right-hand half.
            // copy the first part of the RH-half
            for index in mid + 1..entry_insert {
                target.append_entry(self.get_entry(index));
            }
            target.append_entry(incident_entry);
            for index in entry_insert..self.num_entries() as usize {
                target.append_entry(self.get_entry(index));
            }

            emitted_entry = self.get_entry(mid);

            self.delete_entries(mid..num_entries as usize)
        }

        target.put_entry(0, PageEntry {
            pointer: emitted_entry.pointer, // the emitted entry will take on P\prime as a pointer.
            key: 0, // 0th entries can't have keys.
        });

        emitted_entry
    }
}

pub mod layout {
    pub const HEADER_OFFSET: usize = 0;
    pub const KEY_OFFSET: usize = 1;
    pub const CHILD_OFFSET: usize = 64;

    pub const LEAF_KEYS: usize = 126;
    pub const LEAF_ENTRIES: usize = 127;
    pub const PARENT_KEYS: usize = 63;
    /// If PARENT_KEYS is 2k+1 then REBALANCE_KEYS should be k, although it can be tuned.
    pub const REBALANCE_ENTRIES: usize = 31;
    pub const PARENT_CHILDREN: usize = 64;

    pub const HEADER_FLAGS: usize = 2;
    pub const HEADER_FLAG_LEAF: u8 = 0b0000_0001;
    pub const HEADER_NUMENTRIES: usize = 3;

    pub const MAX_STACK_DEPTH: usize = 64;

}

/// Retrieves a key from the database.
pub fn retrieve<L: StorageLayer<E>, E, S: Searcher<E>>(layer: &L, search: &S, pb: &mut PageBuf) -> Result<Option<TreeKey>, E> {
    let mut p = layer.get_root_id()?;

    loop {
        if p == UNDEFINED_PAGE {
            return Ok(None); // Not in Index
        }

        layer.get_page(p, pb)?;
        let page = Page::from_buf(pb);

        /*
        if y < x_1
            p = p_0
        else if there exists i such that y = x_i
            STOP — Found
        else if there exists i such that x_i < y < x_{i+1}
            p = p_i
        else
            p = p_last
        */

        match page.chop(search)? {
            None => {
                return Ok(None);
            }
            Some(ChopResult::PotentialChildOf(child_index)) => {
                // descend further into the tree
                let entry = page.get_entry(child_index);
                if entry.pointer == UNDEFINED_PAGE {
                    // this page doesn't have children (it's a leaf)
                    return Ok(None);
                } else {
                    p = entry.pointer;
                }
            }
            Some(ChopResult::Equal(key_index)) => {
                let entry = page.get_entry(key_index);
                return Ok(Some(entry.key));
            }
        }
    }
}

#[derive(Clone, Copy)]
pub struct RetrieveStackEntry {
    pub page_id: PageId,
    pub child_index: u8,
}

impl Default for RetrieveStackEntry {
    fn default() -> Self {
        RetrieveStackEntry {
            page_id: 0,
            child_index: 0,
        }
    }
}

pub fn retrieve_cursored<'a, L: StorageLayer<E>, E, S: Searcher<E>>(layer: &L, search: &S, pb: &mut PageBuf, cursor: &mut PageCursor) -> Result<CursoredRetrieval, E> {
    let mut p = layer.get_root_id()?;

    use self::CursoredRetrieval::*;

    cursor.reset(p);

    loop {
        if p == UNDEFINED_PAGE {
            return Ok(NotFound { insert_at: 0 }); // Not in Index
        }

        layer.get_page(p, pb)?;
        let page = Page::from_buf(pb);

        match page.chop(search)? {
            None => {
                return Ok(NotFound { insert_at: 0 });
            }
            Some(ChopResult::PotentialChildOf(child_index)) => {
                // descend further into the tree
                let entry = page.get_entry(child_index);
                if entry.pointer == UNDEFINED_PAGE {
                    // this page doesn't have children, so we are being told lies!
                    // Note that the insertion index should be to the right of this entry, so +1.
                    return Ok(NotFound { insert_at: child_index + 1 });
                } else {
                    p = entry.pointer;
                    cursor.descend_into_page(child_index, p);
                }
            }
            Some(ChopResult::Equal(key_index)) => {
                let entry = page.get_entry(key_index);
                return Ok(Found { key: entry.key, key_index });
            }
        }
    }
}


pub fn insert<L: StorageLayer<E>, E, C: Comparator<E>>(layer: &mut L, compare: &C, pb: &mut PageBuf, insertee: TreeKey) -> Failable<E> {
    let kcsearch = KeyCompareSearch {
        key: insertee,
        compare,
    };

    let mut cursor = PageCursor::new(UNDEFINED_PAGE);
    match retrieve_cursored(layer, &kcsearch, pb, &mut cursor)? {
        CursoredRetrieval::Found { .. } => {
            // it's already there, no need to add
            Ok(())
        }
        CursoredRetrieval::NotFound { insert_at } => {
            // insert the item.
            if cursor.current() == UNDEFINED_PAGE {
                // tree is empty, create new root page with insertee
                let new_root = layer.alloc_page()?;
                cursor.reset(new_root);
                layer.get_page(new_root, pb)?;
                {
                    let mut root_page = Page::create_in_buf(pb, true);

                    root_page.append_entry(PageEntry {
                        pointer: UNDEFINED_PAGE,
                        key: 0,
                    });

                    root_page.append_entry(PageEntry {
                        pointer: UNDEFINED_PAGE,
                        key: insertee,
                    })
                }
                layer.put_page(new_root, pb)?;
                layer.put_root_id(new_root)?;

                // inserted!
                Ok(())
            } else {
                // insert into an existing leaf.

                // TODO finish up this replacement here
                layer.get_page(cursor.current(), pb)?;
                let fission_required = {
                    let mut leaf = Page::from_buf(pb);

                    let insertee = PageEntry {
                        pointer: UNDEFINED_PAGE,
                        key: insertee,
                    };

                    if leaf.num_entries() < leaf.max_entries() as u8 {
                        leaf.insert_entry(insert_at, insertee);
                        false
                    } else {
                        true
                    }
                };
                if !fission_required {
                    layer.put_page(cursor.current(), pb)?;
                }

                if fission_required {
                    let mut pb2 = [0; 128];

                    let mut incident_entry = PageEntry {
                        pointer: UNDEFINED_PAGE,
                        key: insertee,
                    };
                    let mut incident_index = insert_at;

                    loop {
                        layer.get_page(cursor.current(), pb)?;
                        let pprime_id = layer.alloc_page()?;
                        layer.get_page(pprime_id, &mut pb2)?;

                        let mut emitted_entry;

                        {
                            let mut curpage = Page::from_buf(pb);
                            let mut tarpage = Page::create_in_buf(&mut pb2, curpage.is_leaf());

                            emitted_entry = curpage.fission(&mut tarpage, incident_entry, incident_index);
                            // its pointer value has already been moved to P\prime.
                            // it should now point to P\prime.
                            emitted_entry.pointer = pprime_id;
                        }
                        layer.put_page(cursor.current(), pb)?;
                        layer.put_page(pprime_id, &mut pb2)?;

                        if cursor.len() > 0 {
                            // move up to the parent
                            let parent = cursor.ascend_parent();

                            incident_entry = emitted_entry;
                            incident_index = parent.child_index as usize;
                        } else {
                            // no more parent pages -- time to create a new root!
                            let new_root_id = layer.alloc_page()?;
                            let old_root_id = layer.get_root_id()?;
                            {
                                let mut new_root = Page::create_in_buf(&mut pb2, false);
                                new_root.append_entry(PageEntry {
                                    pointer: old_root_id,
                                    key: 0, // 0th entry can't have a key.
                                });
                                new_root.append_entry(emitted_entry);
                            }
                            layer.put_page(new_root_id, &mut pb2)?;
                            layer.put_root_id(new_root_id)?;
                            break;
                        }
                    }
                }

                Ok(())
            }
        }
    }
}


pub fn delete<L: StorageLayer<E>, E, C: Comparator<E>>(layer: &mut L, compare: &C, pb: &mut PageBuf, deletee: TreeKey) -> Failable<E> {
    let search: KeyCompareSearch<E> = super::KeyCompareSearch {
        key: deletee,
        compare,
    };
    let mut cursor = PageCursor::new(0);

    match retrieve_cursored(layer, &search, pb, &mut cursor)? {
        CursoredRetrieval::Found { key_index, .. } => {
            let deletion_page_id = cursor.current();
            assert_ne!(deletion_page_id, UNDEFINED_PAGE, "Undefined page but found... wat?");
            assert_ne!(key_index, 0, "Logic fault: Keys can't exist at idx=0.");

            layer.get_page(deletion_page_id, pb)?;

            let deletee_pointer =
                Page::from_buf(pb).get_entry(key_index).pointer;

            let target_deletion = if deletee_pointer == UNDEFINED_PAGE {
                // the page is a leaf — just delete it right from here
                key_index
            } else {
                // it's not a leaf — we must travel down pages to find the leaf, and
                // then pull up the next (by sort order) value into this page.
                cursor.descend_into_page(key_index, deletee_pointer);

                loop {
                    let descend_pointer = Page::from_buf(pb).get_entry(0).pointer;
                    if deletee_pointer == UNDEFINED_PAGE {
                        // we are at a leaf now.

                        let substitute = Page::from_buf(pb).get_entry(1).key;

                        // load the deletion page
                        layer.get_page(deletion_page_id, pb)?;
                        {
                            let mut deletion_page = Page::from_buf(pb);
                            let deletion_key_entry = deletion_page.get_entry(key_index);
                            deletion_page.put_entry(key_index, PageEntry {
                                key: substitute,
                                ..deletion_key_entry
                            });
                        }
                        layer.put_page(deletion_page_id, pb)?;

                        break;
                    } else {
                        cursor.descend_into_page(0, descend_pointer);
                        layer.get_page(descend_pointer, pb)?;
                    }
                }

                1 // we always delete key = 1 in the leaf page (which the cursor has now descended to)
            };

            // now just delete the element from the leaf.

            let final_deletee_page = cursor.current();
            layer.get_page(final_deletee_page, pb)?;
            let rebalance_required = {
                let mut leaf = Page::from_buf(pb);
                leaf.delete_entries(target_deletion..target_deletion + 1);
                leaf.num_entries() < layout::REBALANCE_ENTRIES as u8
            };
            layer.put_page(final_deletee_page, pb)?;

            if rebalance_required {
                rebalance(layer, pb, &mut cursor)?;
            }

            Ok(())
        }
        CursoredRetrieval::NotFound { .. } => {
            // object not found — nothing wrong with that.
            Ok(())
        }
    }
}

pub fn rebalance<L: StorageLayer<E>, E>(layer: &mut L, pb: &mut PageBuf, cursor: &mut PageCursor) -> Failable<E> {
    type ParentAltered = bool;

    struct PageRebalanceSpec {
        left_id: PageId,
        right_id: PageId,
        left_length: usize,
        right_length: usize,
        maximum_entries: usize,
        /// Note: this must point to the LEFT page.
        parent_curele: PageCursorElement
    }

    fn rebalance_two_pages<L: StorageLayer<E>, E>(layer: &mut L, pb: &mut PageBuf, rebal_spec: PageRebalanceSpec) -> Result<ParentAltered, E> {
        // rebalance the left and right page together.

        // we will perform an in-memory catenation and then re-split if this is an underflow.

        let catenation_len = rebal_spec.left_length + rebal_spec.right_length;
        let blank_entry = PageEntry { pointer: 0, key: 0 };

        // Each full leaf can have LEAF_ENTRIES and a leaf to be rebalanced will have REBALANCE_ENTRIES
        const MAX_REFLOW_LEN: usize = layout::LEAF_ENTRIES + layout::REBALANCE_ENTRIES;
        assert!(catenation_len < MAX_REFLOW_LEN, "Logic fault: catenation length {} is less than the maximum reflow length.", catenation_len);

        let mut catenation_buf = [blank_entry; MAX_REFLOW_LEN];

        let parent_right_index = rebal_spec.parent_curele.child_index as usize + 1;

        layer.get_page(rebal_spec.left_id, pb)?;
        {
            let left_page = Page::from_buf(pb);
            assert_eq!(left_page.num_entries() as usize, rebal_spec.left_length);
            for index in 0..rebal_spec.left_length {
                catenation_buf[index] = left_page.get_entry(index);
            }
        }
        layer.get_page(rebal_spec.right_id, pb)?;
        {
            let right_page = Page::from_buf(pb);
            assert_eq!(right_page.num_entries() as usize, rebal_spec.right_length);
            for index in 0..rebal_spec.right_length {
                catenation_buf[rebal_spec.left_length + index] = right_page.get_entry(index);
            }
        }
        layer.get_page(rebal_spec.parent_curele.page, pb)?;
        {
            let parent_page = Page::from_buf(pb);
            let old_mid = &mut catenation_buf[rebal_spec.left_length];
            assert_eq!(old_mid.pointer, UNDEFINED_PAGE, "Logic fault: old_mid should be an old 0th entry -- with no pointer.");
            let right_entry = parent_page.get_entry(parent_right_index);
            assert_eq!(right_entry.pointer, rebal_spec.right_id, "Logic fault: parent[child index + 1].pointer should be right.");
            old_mid.key = right_entry.key;
        }

        // Now the in-memory catenation is done.


        if rebal_spec.left_length + rebal_spec.right_length < rebal_spec.maximum_entries {
            // do a catenation — merge the two pages together, and delete the parent's right-entry (P\prime).

            {
                let mut parent_page = Page::from_buf(pb);
                parent_page.delete_entries(parent_right_index..parent_right_index+1);
            }
            layer.put_page(rebal_spec.parent_curele.page, pb)?;

            layer.get_page(rebal_spec.left_id, pb)?;
            {
                let mut left_page = Page::from_buf(pb);
                left_page.delete_entries(0..rebal_spec.left_length);
                for index in 0..catenation_len {
                    left_page.append_entry(catenation_buf[index]);
                }
            }
            layer.put_page(rebal_spec.left_id, pb)?;

            // delete right page (P\prime)
            layer.free_page(rebal_spec.right_id)?;

            Ok(true) // parent reflow may be necessary
        } else {
            // too large for catenation ­ do an underflow
            let new_left_len = catenation_len / 2;

            {
                let mut parent_page = Page::from_buf(pb);
                let new_mid = &mut catenation_buf[new_left_len];
                let mut right_entry = parent_page.get_entry(parent_right_index);
                right_entry.key = new_mid.key;
                parent_page.put_entry(parent_right_index, right_entry);
                assert_eq!(right_entry.pointer, rebal_spec.right_id, "Logic fault: parent[child index + 1].pointer should be right.");
                new_mid.key = UNDEFINED_PAGE;
            }
            layer.put_page(rebal_spec.parent_curele.page, pb)?;

            layer.get_page(rebal_spec.left_id, pb)?;
            {
                let mut left_page = Page::from_buf(pb);
                left_page.delete_entries(0..rebal_spec.left_length);
                for index in 0..new_left_len {
                    left_page.append_entry(catenation_buf[index]);
                }
            }
            layer.put_page(rebal_spec.left_id, pb)?;

            layer.get_page(rebal_spec.right_id, pb)?;
            {
                let mut right_page = Page::from_buf(pb);
                right_page.delete_entries(0..rebal_spec.right_length);
                for index in new_left_len..catenation_len {
                    right_page.append_entry(catenation_buf[index]);
                }
            }
            layer.put_page(rebal_spec.right_id, pb)?;

            // UNDERFLOW complete!
            Ok(false) // no parent reflow necessary
        }
    }

    loop {
        let rebalance_page_id = cursor.current();
        layer.get_page(rebalance_page_id, pb)?;
        {
            let num_entries;
            let max_entries;
            {
                let page = Page::from_buf(pb);
                num_entries = page.num_entries();
                max_entries = page.max_entries();
            }

            if num_entries < layout::REBALANCE_ENTRIES as u8 {
                // this page needs to be rebalanced!

                if cursor.len() <= 0 {
                    // this page is the root — it cannot be rebalanced.
                    return Ok(());
                }

                let PageCursorElement {
                    child_index: rebalance_page_index_in_parent,
                    page: parent_id
                } = cursor.ascend_parent();


                let left_length;
                let right_length;

                let left_id;
                let right_id;

                let mut pb2 = [0; 128];
                layer.get_page(parent_id, &mut pb2)?;
                {
                    let parent = Page::from_buf(&mut pb2);

                    if rebalance_page_index_in_parent > 0 {
                        let left_sibling = rebalance_page_index_in_parent - 1;
                        left_id = parent.get_entry(left_sibling as usize).pointer;
                    } else {
                        left_id = UNDEFINED_PAGE;
                    }

                    if rebalance_page_index_in_parent <= parent.num_entries() {
                        let right_sibling = rebalance_page_index_in_parent + 1;
                        right_id = parent.get_entry(right_sibling as usize).pointer;
                    } else {
                        right_id = UNDEFINED_PAGE;
                    }
                }

                if left_id == UNDEFINED_PAGE {
                    left_length = 0xFF; // u8 max
                } else {
                    layer.get_page(left_id, &mut pb2)?;
                    let left_page = Page::from_buf(&mut pb2);
                    left_length = left_page.num_entries();
                }
                if right_id == UNDEFINED_PAGE {
                    right_length = 0xFF; // u8 max
                } else {
                    layer.get_page(right_id, &mut pb2)?;
                    let right_page = Page::from_buf(&mut pb2);
                    right_length = right_page.num_entries();
                }

                assert!(left_length < 0xFF || right_length < 0xFF, "Both left and right pages do not exist, but this isn't a root page.");
                let rebal_spec = if left_length < right_length {
                    PageRebalanceSpec {
                        left_id,
                        right_id: rebalance_page_id,
                        left_length: left_length as usize,
                        right_length: num_entries as usize,
                        maximum_entries: max_entries,
                        parent_curele: PageCursorElement {
                            page: parent_id,
                            child_index: rebalance_page_index_in_parent - 1,
                        },
                    }
                } else {
                    PageRebalanceSpec {
                        left_id: rebalance_page_id,
                        right_id,
                        left_length: num_entries as usize,
                        right_length: right_length as usize,
                        maximum_entries: max_entries,
                        parent_curele: PageCursorElement {
                            page: parent_id,
                            child_index: rebalance_page_index_in_parent,
                        },
                    }
                };

                if ! rebalance_two_pages(layer, pb, rebal_spec)? {
                    // parent does not need rebalancing
                    return Ok(());
                }
            } else {
                return Ok(());
            }
        }
    }
}
