# pluggable_btree

A pluggable B-Tree Library for Rust.

Intended for use in `no_std` environments although this may be delayed for a couple of versions initially.

## Requirements for using the library

You have to provide the following functionality:

* a way to load and save pages by ID.
* a way to load and save the root page ID.
* a way to allocate (find unused page IDs) and free pages.

It is recommended that loading and saving pages is used with a cache, preferrably with a minimum capacity of 3 pages.

Each page is 512 bytes long.

## Licence

MIT Licence.

© 2018 Olivier 'Reivilibre'

