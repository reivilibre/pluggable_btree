#![feature(never_type)]
extern crate pluggable_btree;
use pluggable_btree::{BlockBTree, Comparator, StorageLayer};

use std::collections::HashMap;
use std::error::Error;
use std::cmp::Ordering;


type Page = [u32; 128];

struct MapAccessLayer {
    pub root_id: u32,
    pub pages: HashMap<u32, Page>,
}
impl StorageLayer<Box<Error>> for MapAccessLayer {
    fn get_root_id(&self) -> Result<u32, Box<Error>> {
        Ok(self.root_id)
    }

    fn put_root_id(&mut self, root_id: u32) -> Result<(), Box<Error>> {
        self.root_id = root_id;
        Ok(())
    }

    fn get_page(&self, page_id: u32, buffer: &mut [u32; 128]) -> Result<(), Box<Error>> {
        let page = self.pages.get(&page_id).ok_or("Page not found")?;
        buffer.copy_from_slice(page);
        Ok(())
    }

    fn put_page(&mut self, page_id: u32, buffer: &mut [u32; 128]) -> Result<(), Box<Error>> {
        let page = self.pages.get_mut(&page_id).ok_or("Page not allocated")?;
        page.copy_from_slice(buffer);
        Ok(())
    }

    fn alloc_page(&mut self) -> Result<u32, Box<Error>> {
        for i in 1..=42 {
            if ! self.pages.contains_key(&i) {
                self.pages.insert(i, [0; 128]);
                return Ok(i);
            }
        }
        Err("Not great")?;
        unreachable!()
    }

    fn free_page(&mut self, page_id: u32) -> Result<(), Box<Error>> {
        if self.pages.contains_key(&page_id) {
            self.pages.remove(&page_id);
            return Ok(())
        } else {
            Err("Page is already free")?;
        }
        unreachable!()
    }
}



fn print_nonzero(map: &MapAccessLayer) {
    println!("Root: {}", map.root_id);
    for i in 1..=42 {
        if map.pages.contains_key(&i) {
            println!("==================== Page {:2} ====================", i);
            let page = map.pages.get(&i).unwrap();
            for (i, word) in page.iter().enumerate() {
                if i % 16 == 0 {
                    println!();
                }
                print!("{:08X} ", word);
            }
            println!("\n***************************************************")
        }
    }
}


struct RawComparator<E> {
    _wat: std::marker::PhantomData<E>
}
impl<E> Comparator<E> for RawComparator<E> {
    fn compare(&self, a: u32, b: u32) -> Result<Ordering, E> {
        Ok(a.cmp(&b))
    }
}

fn main() {
    println!("Creating storage.");

    let mut layer = MapAccessLayer {
        root_id: 0,
        pages: HashMap::new()
    };

    println!("Printing non-zero pages...");
    print_nonzero(&layer);

    let mut tree = BlockBTree {
        storage: &mut layer,
        comparator: &RawComparator{_wat: Default::default()},
        _wat: std::marker::PhantomData{},
    };

    println!("Inserting 42, 43, 41.");
    tree.insert(42).unwrap();
    tree.insert(43).unwrap();
    tree.insert(41).unwrap();

    print_nonzero(&tree.storage);

    println!("Inserting 101...201.");

    for i in 101..=201 {
        tree.insert(i).unwrap();
    }

    print_nonzero(&tree.storage);

    println!("Inserting 1337...1392.");

    for i in 1337..=1392 {
        tree.insert(i).unwrap();
    }

    print_nonzero(&tree.storage);

    println!("Deleting 162.");
    tree.delete(162).unwrap();
    print_nonzero(&tree.storage);

    println!("Deleting 110..120.");
    for i in 110..120 {
        tree.delete(i).unwrap();
    }
    print_nonzero(&tree.storage);

    println!("Deleting 130..140.");
    for i in 130..140 {
        tree.delete(i).unwrap();
    }
    print_nonzero(&tree.storage);

    println!("Deleting 170..190.");
    for i in 170..190 {
        tree.delete(i).unwrap();
    }
    print_nonzero(&tree.storage);

    println!("Deleting 1347..1367.");
    for i in 1347..1367 {
        tree.delete(i).unwrap();
    }
    print_nonzero(&tree.storage);

    println!("Deleting 0x7A..0x91.");
    for i in 0x7A..0x91 {
        tree.delete(i).unwrap();
    }
    print_nonzero(&tree.storage);

    println!("About to catenate:");
    tree.delete(0x91).unwrap();
    print_nonzero(&tree.storage);
}